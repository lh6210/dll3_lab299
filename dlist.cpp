
#include "CS299_dlist.h"


int list::display_last_two()
{
    if (!head)
        return 0;

    else if (!head->next)
        return head->data;

    // at least two nodes in the list
    return display_last_two_prog(head);

}

int list::display_last_two_prog(node* nd)
{
    // base case
    if (nd->next == tail)
    {
        // display the last two nodes
        int item1 = nd->data;
        int item2 = tail->data;
        cout << item2 << " -> " << item1 << endl;
        int result = item1 + item2;
        node* pvNode = tail;
        nd->prev->next = pvNode;
        pvNode->prev = nd->prev;
        pvNode->next = nd;
        nd->prev = pvNode;
        nd->next = NULL;
        tail = nd;
        return result;
    }

    return display_last_two_prog(nd->next);
}

int list::remove_last_two()
{
    if (!head)
        return 0;
    // one node
    if (!head->next)
    {
        int dt = head->data;
        delete head;
        head = NULL;
        tail = NULL;
        return dt;
    }

    // at least two nodes
    return remove_last_two_prog(head);
}

int list::remove_last_two_prog(node*& nd)
{
    // the last two nodes
    if (nd->next == tail)
    {
        //tail->prev = NULL;
        delete nd->next;
        tail = nd->prev;
        delete nd;
        tail->next = NULL;
        nd = NULL;
        return 0;
    }

    return nd->data + remove_last_two_prog(nd->next);

}

float list::add_average()
{
    // empty list
    if (!head)
        return 0;

    // only one node
    if (!head->next)
    {
        int avg = head->data;
        head->next = new node;
        tail = head->next;
        tail->prev = head;
        tail->next = NULL;
        tail->data = avg;
        return avg;
    }

    int sum = 0;
    int count = 0;
    return add_average_prog(head, sum, count); 

}

float list::add_average_prog(node*& nd, int& sum, int& ct)
{
    sum += nd->data;
    ct++;
    float avg = sum / ct;

    if (!nd->next)
    {
        nd->next = new node;
        tail = nd->next;
        tail->prev = nd;
        tail->next = NULL;
        tail->data = avg;
        return avg;
    }

    return add_average_prog(nd->next, sum, ct);

}

int list::remove_largest_two()
{
    // empty list
    if (!head)
        return 0;

    // only one node
    if (!head->next)
    {
        int sum = head->data;
        delete head;
        head->next = NULL;
        tail->prev = NULL;
        return sum;
    }

    // only two nodes in the list
    if (head->next == tail)
    {
        int sum = tail->data;
        delete tail;
        tail->prev = NULL;
        sum += head->data;
        delete head;
        head->next = NULL;
        return sum;
    }

    node* maxNd;
    node* secNd;

    // assign values to the two pointers
    if (head->data >= head->next->data)
    {
        maxNd = head;
        secNd = head->next;
    }
    else 
    {
        maxNd = head->next;
        secNd = head;
    }

    return remove_largest_two_prog(head->next->next, maxNd, secNd);
}

int list::remove_largest_two_prog(node*& nd, node*& first, node*& second)
{
    int num_max = first->data;
    int num_second = second->data;
    // ending condition
    if (!nd)
    {
        // first is at the end
        if (!first->next)
        {
            tail = first->prev;
            tail->next = NULL;
            delete first;
            first->prev = NULL;
            first->next = NULL;
        }
        // first is at the beginning
        else if (!first->prev)
        {
            head = first->next;
            head->prev = NULL;
            delete first;
            first->next = NULL;
            first->prev = NULL;
        }
        else  // first is the middle node
        {
            first->prev->next = first->next;
            first->next->prev = first->prev;
            delete first;
            first->prev = NULL;
            first->next = NULL;
        }
            
        // second is at the end
        if (!second->next)
        {
            tail = second->prev;
            tail->next = NULL;
            delete second;
            second->prev = NULL;
            second->next = NULL;
        }
        // second is at the beginning
        else if (!second->prev)
        {
            head = second->next;
            head->prev = NULL;
            delete second;
            second->next = NULL;
            second->prev = NULL;
        }
        else  // second is the middle node
        {
            second->prev->next = second->next;
            second->next->prev = second->prev;
            delete second;
            second->prev = NULL;
            second->next = NULL;
        }

        return num_max + num_second;
    }

    int item = nd->data;
    if (item >= num_max)
    {
        second = first;
        first = nd;
    }
    else if (item >= num_second)
        second = nd;

    return remove_largest_two_prog(nd->next, first, second);

}


