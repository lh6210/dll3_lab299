#include "CS299_dlist.h"
//Please place your name here:
//
//
using namespace std;


int main()
{
    list object;
    object.build();
    object.display();

    //PLEASE PUT YOUR CODE HERE to call the functions
    cout << endl << endl;

    cout << "Test begins: ****************************************" << endl;
    //int result = object.display_last_two();
    //cout << "The sum of the last two nodes is " << result << endl;

    /*
    int result = object.remove_last_two();
    cout << "The sum of the list after removal is " << result << endl;

    float result = object.add_average();
    cout << "The average is " << result <<  endl;
    */

    int result = object.remove_largest_two();
    cout << "The sum of the largest two is " << result << endl;

    cout << "Test ends: ****************************************" << endl;


    object.display();
    
    return 0;
}
