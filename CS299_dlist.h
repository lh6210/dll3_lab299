//doubly linked list
//Notice that there is a head and a tail pointer!!!
#include <iostream>
#include <cstring>
#include <cctype>
#include <cstdlib>
using namespace std;


struct node
{
    int data;		//some questions use a char * data;
    node * prev;
    node * next;
};

class list
{
    public:
        //These functions are already written
        list();         //supplied
        ~list();        //supplied
        void build();   //supplied
        void display(); //supplied

     /* *****************YOUR TURN! ******************************** */
     /* place your prototype here */
        int display_last_two();
        int remove_last_two();
        float add_average();
        int remove_largest_two();
     
     private:
        node * head;   //notice there is both a head
        node * tail;   //and a tail, common for DLL
        int display_last_two_prog(node*);
        int remove_last_two_prog(node*&);
        float add_average_prog(node*&, int&, int&);
        int remove_largest_two_prog(node*&, node*&, node*&);
};
